package org.example.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "genre")
public class Genre {

    @Id
    private String name;

    @Column(name = "is_active")
    private boolean active = true;

    @OneToMany(mappedBy = "genre", fetch = FetchType.LAZY)
    private List<Book> books = new ArrayList<>();

    @ElementCollection
    @CollectionTable(name = "genre_prop", joinColumns = @JoinColumn(name = "genre"))
    private List<String> props = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Book> getBooks() {
        return books;
    }

    public List<String> getProps() {
        return props;
    }

    public void setProps(List<String> props) {
        this.props = props;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "name='" + name + '\'' +
                ", active=" + active +
                ", props=" + props +
                '}';
    }
}
