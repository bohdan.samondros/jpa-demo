package org.example.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@DiscriminatorValue("TEACHER")
public class Teacher extends Person {

    @Column
    private int lections;

    public int getLections() {
        return lections;
    }

    public void setLections(int lections) {
        this.lections = lections;
    }
}
