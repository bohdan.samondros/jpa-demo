package org.example;

import org.example.domain.Book;
import org.example.domain.Genre;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class QueryService {
    private EntityManager entityManager;

    public QueryService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Book> getBooksWhereGenreIsNull() {
        String query = "SELECT b FROM Book b WHERE b.genre IS NULL";
        Query managerQuery = entityManager.createQuery(query);
        return (List<Book>) managerQuery.getResultList();
    }

    public List<Genre> getBooksWhereGenrePropsSizeGreater1() {
        String query = "SELECT g FROM Genre g WHERE g.props.size > 1";
        Query managerQuery = entityManager.createQuery(query);
        return (List<Genre>) managerQuery.getResultList();
    }

    public List<Genre> getBooksWhereGenrePropsSizeGreater12() {
        String query = "SELECT b, g FROM Book b LEFT JOIN b.genre g";
        Query managerQuery = entityManager.createQuery(query);
        return (List<Genre>) managerQuery.getResultList();
    }
}
