package org.example;

import org.example.domain.Book;
import org.example.domain.Genre;
import org.example.domain.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookShop");
        EntityManager entityManager = emf.createEntityManager();

        persistDemo(entityManager);
        findDemo(entityManager);

//        queryDemo(entityManager);

        entityManager.close();
        emf.close();
    }

    private static void persistDemo(EntityManager entityManager) {
        PersistenceService persistenceService = new PersistenceService(entityManager);

        Warehouse warehouse = new Warehouse();
        warehouse.setName("General");

        Genre genre = new Genre();
        genre.setName("Horor");

        Book book = new Book();
        book.setAuthor("Author Name");
        book.setTitle("My favorite book");
        book.setGenre(genre);
        book.setWarehouse(warehouse);

        persistenceService.save(book);

        System.out.println(book);
    }

    public static void findDemo(EntityManager entityManager) {
        Book book = entityManager.find(Book.class, 1L);
        entityManager.detach(book);
        System.out.println("Found: " + book);
    }

    public static void queryDemo(EntityManager entityManager) {
        PersistenceService persistenceService = new PersistenceService(entityManager);
        Book book = new Book();
        book.setAuthor("Author Name");
        book.setTitle("My favorite book");

        Genre genre = new Genre();
        genre.setName("Genre With Props");
        genre.getProps().add("A");
        genre.getProps().add("B");

        persistenceService.save(book);
        persistenceService.save(genre);

        QueryService queryService = new QueryService(entityManager);

        System.out.println(queryService.getBooksWhereGenreIsNull());
        System.out.println(queryService.getBooksWhereGenrePropsSizeGreater1());
    }
}
