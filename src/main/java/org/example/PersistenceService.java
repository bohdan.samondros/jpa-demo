package org.example;

import org.example.domain.Warehouse;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class PersistenceService {

    private Session session;
    private EntityManager entityManager;

    public PersistenceService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public PersistenceService(Session session) {
        this.session = session;
    }


    public <T> void save(T entity) {
        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.persist(entity);

        if (!inTransaction) {
//            entityManager.flush();
            transaction.commit();
        }
    }
}
